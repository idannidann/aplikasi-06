package azra.wildan.idanapp06

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.fragment.app.FragmentTransaction
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), BottomNavigationView.OnNavigationItemSelectedListener {

    lateinit var fragProdi : FragmentProdi
    lateinit var fragMhs : FragmentMahasiswa
    lateinit var fragPerkalian : FragmentPerkalian
    lateinit var fragPembagian : FragmentPembagian
    lateinit var ft : FragmentTransaction

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        bottomNavigationView.setOnNavigationItemSelectedListener(this)
        fragProdi = FragmentProdi()
        fragMhs = FragmentMahasiswa()
        fragPerkalian = FragmentPerkalian()
        fragPembagian = FragmentPembagian()
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            R.id.itemProdi ->{
                ft = supportFragmentManager.beginTransaction()
                ft.replace(R.id.franeLayout,fragProdi).commit()
                franeLayout.setBackgroundColor(Color.argb(245,255,255,225))
                franeLayout.visibility = View.VISIBLE
            }
            R.id.itemMhs ->{
                ft = supportFragmentManager.beginTransaction()
                ft.replace(R.id.franeLayout,fragMhs).commit()
                franeLayout.setBackgroundColor(Color.argb(245,255,255,255))
                franeLayout.visibility = View.VISIBLE
            }
            R.id.itemPerkalian ->{
                ft = supportFragmentManager.beginTransaction()
                ft.replace(R.id.franeLayout,fragPerkalian).commit()
                franeLayout.setBackgroundColor(Color.argb(255,245,245,215))
                franeLayout.visibility = View.VISIBLE
            }
            R.id.itemPembagian ->{
                ft = supportFragmentManager.beginTransaction()
                ft.replace(R.id.franeLayout,fragPembagian).commit()
                franeLayout.setBackgroundColor(Color.argb(255,245,245,215))
                franeLayout.visibility = View.VISIBLE
            }
            R.id.itemAbout -> franeLayout.visibility = View.GONE
        }
        return true
    }
}
